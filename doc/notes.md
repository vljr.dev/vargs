# Notes...

## Validators

Checks the input arguments to see if the argument input is valid. 

Command Line Arguments

Generic Type:
1. IsContained

File IO
1. IsFile
2. IsDirectory
3. IsFileOrDirectory

Numerical 
1. IsWithinRange

Integral Specific
1. IsValueInEnum