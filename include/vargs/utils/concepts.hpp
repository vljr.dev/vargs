#ifndef INCLUDE_VARGS_UTILS
#define INCLUDE_VARGS_UTILS

#include <concepts>

namespace vargs::utils {

template <typename T>
concept Numerical=std::integral<T> || std::floating_point<T>;

}

#endif /* INCLUDE_VARGS_UTILS */
