#ifndef INCLUDE_VARGS_ARGUMENT_FORMAT_MESSAGES
#define INCLUDE_VARGS_ARGUMENT_FORMAT_MESSAGES

#include <string_view>

namespace vargs {

/**
 * @brief Defines the argument format messages 
 * 
 * @tparam StringType The type used to store the format messages
 */
template <typename StringType=std::string_view>
struct ArgumentFormatMessages
{
  /**
   * @brief Construct a new Argument Formatters object
   * 
   * @param help_formatter The help formatter
   * @param opt_formatter The optional formatter
   * @param pos_formatter The positional formatter
   * @param usage_formatter The usage formatter
   */
  ArgumentFormatMessages(
    StringType help_message_formatter=default_help_message_format,
    StringType version_message_formatter=default_version_format,
    StringType optional_formatter=default_optional_format,
    StringType positional_formatter=default_positional_format,
    StringType usage_formatter=default_usage_format
  ) :
    m_help_message_formatter(help_message_formatter),
    m_version_message_formatter(version_message_formatter),
    m_optional_formatter(optional_formatter),
    m_positional_formatter(positional_formatter),
    m_usage_formatter(usage_formatter)
  {}

  /// @brief The help message formatter
  StringType m_help_message_formatter;

  /// @brief The version message formatter
  StringType m_version_message_formatter;

  /// @brief The optional argument formatter
  StringType m_optional_formatter;

  /// @brief The positional argument formatter
  StringType m_positional_formatter;

  /// @brief The usage formatter
  StringType m_usage_formatter;

private:
  // Default formats...

  static inline constexpr std::string_view default_help_message_format = 
    "{usage_statement}\n{version}{description}{args}{positional_arguments}{epilog}"
  ;

  static inline constexpr std::string_view default_version_format = 
    "{version}"
  ;

  static inline constexpr std::string_view default_optional_format = 
    "{short_argument},{long_argument}{arg_spacing}{type}{arg_desc}{default_or_required}"
  ;

  static inline constexpr std::string_view default_positional_format = 
    "{argument} {positions}{arg_spacing}{type}{arg_desc}"
  ;

  static inline constexpr std::string_view default_usage_format = 
    "{usage_prefix}{prog_name}{args}{positional_arguments}"
  ;
};

}

#endif /* INCLUDE_VARGS_ARGUMENT_FORMAT_MESSAGES */
