#ifndef INCLUDE_VARGS_CASTS_CASTS
#define INCLUDE_VARGS_CASTS_CASTS

#include <charconv>
#include <concepts>
#include <filesystem>
#include <optional>
#include <string_view>

#include <tuple>
#include <vargs/utils/concepts.hpp>

namespace vargs::casts {

/**
 * @brief Converts a string input to an integral value
 * 
 * @tparam T 
 * @param sv The string view value
 * @param base The base of the string version of the integral
 *
 * @return std::tuple<T, std::errc> The value, and associated error code
 */
template <std::integral T>
std::tuple<T, std::errc> from_string_view(std::string_view sv, int base=10)
{
  T result=T();
  std::errc err_code = std::from_chars(sv.begin(), sv.end(), result, base).ec;
  return std::make_tuple(result, err_code);
}

/**
 * @brief Converts a string input to a floating point value
 * 
 * @tparam T 
 * @param sv The string view value
 * @param fmt The format used to make the conversion
 *
 * @return std::tuple<T, std::errc> The value, and associated error code
 */
template <std::floating_point T>
std::tuple<T, std::errc> from_string_view(std::string_view sv, std::chars_format fmt=std::chars_format::general)
{
  T result=T();
  std::errc err_code = std::from_chars(sv.begin(), sv.end(), result, fmt).ec;
  return std::make_tuple(result, err_code);
}

/**
 * @brief Converts a string input to a path
 * 
 * @tparam T 
 *
 * @param sv The string view value
 * @param fmt The format of the path (default is automatically derived)
 *
 * @return std::filesystem::path The path of the input string view
 */
template <std::same_as<std::filesystem::path> T>
T from_string_view(std::string_view sv, std::filesystem::path::format fmt = std::filesystem::path::auto_format)
{
  return std::filesystem::path(sv, fmt);
}

/**
 * @brief Dummy convert... Used to generically get string views, if desired...
 * 
 * @tparam T 
 *
 * @param sv The string view value
 *
 * @return T The input string view value
 */
template <std::same_as<std::string_view> T>
T from_string_view(std::string_view sv) { return sv; }

/**
 * @brief Converts a std::string_view to a string, if desired
 * 
 * @tparam T 
 *
 * @param sv The string view value
 *
 * @return T The input string_view now as a string
 */
template <std::same_as<std::string> T>
T from_string_view(std::string_view sv) { return std::string(sv); }

/**
 * @brief Converts an input to a string
 * 
 * @tparam T 
 * @param t The parameter to convert to a string
 *
 * @return std::string A string representation of the input
 */
template <typename T>
requires requires (T t) {{std::to_string(t)} -> std::same_as<std::string>; }
std::string to_string(T val) { return std::to_string(val); }

/**
 * @brief Converts an input to a string
 * 
 * @tparam T 
 * @param t The parameter to convert to a string
 *
 * @return std::string A string representation of the input
 */
template <typename T>
requires requires (T t) {{t.string()} -> std::same_as<std::string>; }
std::string to_string(T val) { return val.string(); }

/**
 * @brief Converts an input to a string
 * 
 * @tparam T 
 * @param t The parameter to convert to a string
 *
 * @return std::string A string representation of the input
 */
template <typename T>
requires requires (T t) {{t.str()} -> std::same_as<std::string>; }
std::string to_string(T val) { return val.str(); }

/**
 * @brief Converts an input to a string
 * 
 * @tparam T 
 * @param t The parameter to convert to a string
 *
 * @return std::string A string representation of the input
 */
template <typename T>
requires requires (T t) {{t.toString()} -> std::same_as<std::string>; }
std::string to_string(T val) { return val.toString(); }

/**
 * @brief Converts a string to a ... string... for completeness
 * 
 * @param s A string to cast to a string (does nothing)
 *
 * @return std::string The string
 */
template <std::same_as<std::string> T>
inline std::string to_string(T const& s) { return std::move(s); }

/**
 * @brief Converts a string_view to a string
 * 
 * @param s The string view to convert to a string
 *
 * @return std::string 
 */
template <std::same_as<std::string_view> T>
inline std::string to_string(T s) { return std::string(s); }

}

#endif /* INCLUDE_VARGS_CASTS_CASTS */
