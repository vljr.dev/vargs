#ifndef INCLUDE_VARGS_ARGUMENT_PARSER_PREFERENCES
#define INCLUDE_VARGS_ARGUMENT_PARSER_PREFERENCES

#include <string>

namespace vargs {

/**
 * @brief Defines preferences for 
 * 
 */
template <typename StringType=std::string>
struct ArgumentParserPreferences
{
  using string_t = StringType;

  ArgumentParserPreferences(
    bool show_help_no_arguments_passed=false
  ) :
    m_show_help_no_arguments_passed(show_help_no_arguments_passed)
  {}

  /// @brief Show the help message by default when no arguments are passed into the program
  bool m_show_help_no_arguments_passed;

  /// @brief The default program info group name
  constexpr static inline string_t m_default_info_group="program_info";
};

}

#endif /* INCLUDE_VARGS_ARGUMENT_PARSER_PREFERENCES */
