#ifndef INCLUDE_VARGS_ERROR_MSG
#define INCLUDE_VARGS_ERROR_MSG

#include <concepts>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <ostream>
#include <string>
#include <system_error>

namespace vargs {

namespace detail
{
  /**
   * @brief Indicates if the string is empty
   * 
   * @tparam T 
   * @param x The string to test, assuming it has an empty method
   * @return true if it is empty, otherwise false
   */
  template <typename T>
  requires requires(T x) { {x.empty()} -> std::same_as<bool>; }
  inline bool empty(T&& str) noexcept
  {
    return str.empty();
  }

  /**
   * @brief Indicates if a c style string is empty
   * 
   * @param str The string to test
   * @return true if it is empty, otherwise false
   */
  inline bool empty(char const* str) noexcept
  {
    return (strlen(str) == 0);
  }
}

/**
 * @brief Proxy class around an error code. Allows for operator= support for error codes
 * 
 * @tparam ErrorCodeType 
 */
template <std::integral ErrorCodeType=uint8_t>
struct ErrorCodeProxy
{
/// @brief The type used to store the underlying error code
public: using value_type = ErrorCodeType;
/// @brief Shorthand for referencing this class
private: using class_t = ErrorCodeProxy<ErrorCodeType>;

public:
  /**
   * @brief Construct a new Error Code Proxy object
   * 
   * @param code The error code
   */
  ErrorCodeProxy(value_type code=value_type()) noexcept : m_code(code) {}

  /**
   * @brief Construct a new Error Code Proxy object from std::errc
   * 
   * @param code The error code
   */
  ErrorCodeProxy(std::errc code) : m_code(toIntegralCode(code)) {}

  /**
   * @brief Defines operator= for the error code
   * 
   * @param code The error code
   *
   * @return class_t This object
   */
  class_t& operator=(value_type code) noexcept { m_code = code; return *this; }

  /**
   * @brief 
   * 
   * @param code 
   * @return class_t& 
   */
  class_t& operator=(std::errc code) { m_code = toIntegralCode(code); return *this; }

  /**
   * @brief Casts the error to an integral
   *
   * @details Allow for implicit conversions from an ErrorCodeProxy to an integral type
   * 
   * @tparam T 
   * @return T The integral version of the error code
   */
  template <std::integral T>
  operator T() const noexcept { return m_code; }

  /**
   * @brief Casts the error code to a boolean
   * 
   * @return true The error code shows an error
   * @return false The error code does not show an error
   */
  explicit operator bool() const noexcept { return static_cast<bool>(m_code); }

  /**
   * @brief operator! 
   * 
   * @return true The error code does not show an error
   * @return false The error code shows an error
   */
  bool operator!() const noexcept { return !static_cast<bool>(*this); }

  /**
   * @brief Writes the code to the output stream
   * 
   * @param os The output stream
   * @return std::ostream& The output stream
   */
  std::ostream& operator<< (std::ostream& os)
  {
    // Ensure that we are printing out an integral, and not a char character
    if constexpr(sizeof(value_type) == 1)
    {
      os << static_cast<int>(m_code);
    }
    else 
    {
      os << m_code;
    }
    return os;
  }

  /// @brief The error code
  uint8_t m_code;

private:
  static value_type toIntegralCode(std::errc code) { return static_cast<value_type>(code); }
};

/// @brief The error code proxy type for most use cases without the need of a template
using error_code_t = ErrorCodeProxy<>;

/**
 * @brief Defines an error message to be displayed in the event of an error
 * 
 * @tparam StringType The string type used to store an error message
 * @tparam ErrorCodeType The error code type to store the error code
 */
template <typename StringType=std::string, typename ErrorCodeType=error_code_t>
struct ErrorMessage
{
public:
  using message_t = StringType;
  using code_t = ErrorCodeType;

private: using class_t = ErrorMessage<StringType, ErrorCodeType>;

public:
  /**
  * @brief Construct a new Error Message object
  * 
  * @param error_string The error string
  * @param code The error code
  */
  explicit constexpr ErrorMessage(message_t const& error_string=message_t(), code_t const& code=EXIT_SUCCESS) :
    m_message(error_string),
    m_code(code)
  {}

  /**
  * @brief Construct a new Error Message object
  * 
  * @param code The error code
  */
  explicit constexpr ErrorMessage(code_t code) :
    m_message(message_t()),
    m_code(code)
  {}

  /**
  * @brief Indicates if there is an error. 
  *
  * @details Either the message will be non empty, or the error code will be non-zero
  * 
  * @return true There is an error
  * @return false There is not an error
  */
  operator bool() const noexcept
  {
    return (!detail::empty(m_message) || m_code);
  }

  /**
  * @brief Indicates if there is not an error
  * 
  * @return true There is NOT an error
  * @return false There is an error
  */
  bool operator!() const noexcept { return !static_cast<bool>(*this); }

  /**
   * @brief Casts the error to an integral
   *
   * @details Allow for implicit conversions from an ErrorCodeProxy to an integral type
   * 
   * @tparam T 
   * @return T The integral version of the error code
   */
  template <std::integral T>
  operator T() const noexcept { return static_cast<T>(m_code); }

  /**
   * @brief Writes the message to the output stream
   * 
   * @param os The output stream
   * @return std::ostream& The output stream
   */
  std::ostream& operator<< (std::ostream& os)
  {
    os << m_message;
    return os;
  }

  /**
  * @brief Outputs the error to the terminal if there is an error message
  * 
  * @param to_cerr if True, output to stderr, otherwise to stdout
  */
  class_t output(bool to_cerr=true) const
  {
    if (!detail::empty(m_message)) { (to_cerr) ? (std::cerr << m_message << "\n") : (std::cout << m_message << "\n"); }
    return *this;
  }

  /// @brief The error message
  message_t m_message;

  /// @brief The error code
  code_t m_code;
};

/// @brief Defines a basic error message type without the need of templates
using error_message_t = ErrorMessage<>;
}

#endif /* INCLUDE_VARGS_ERROR_MSG */
