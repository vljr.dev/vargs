#ifndef INCLUDE_VARGS_ARGUMENTS_OPTIONAL_ARGUMENT
#define INCLUDE_VARGS_ARGUMENTS_OPTIONAL_ARGUMENT

#include <cstdint>
#include <string>
#include <string_view>
#include <vector>
#include <variant>

#include <vargs/arguments/base_argument.hpp>
#include <vargs/arguments/split_argument.hpp>
#include <vargs/casts/casts.hpp>


namespace vargs {

/**
 * @brief Defines the argument policy used for a given input argument
 * 
 */
enum class OptionalArgumentPolicy : uint16_t
{
  none, // No follow on argument
  one, // Exactly one argument
  none_or_one, // "?" (Parses to next --arg)
  none_or_more, // "?+" (Parses to next --arg)
  append, // "*" (One param only)
  append_and_required, // "+" (One param only)
  append_multiple, // "++" (Append, can be more than one argument space delimited)
  show_help,
  show_version,
  show_program_info
};

using optional_argument_policy_t = std::variant<std::size_t, OptionalArgumentPolicy>;

template <typename StringType=std::string>
struct OptionalArgument : public BaseArgument<StringType>
{
  using string_t = typename BaseArgument<StringType>::string_t;

  /// Sets the default value (std::string) or flags whether it is required or not 
  using default_required_variant_t = std::variant<string_t, bool>;
  using argument_policy_t = optional_argument_policy_t;

  /**
   * @brief Construct a new Optional Argument object
   * 
   * @param argument The input argument, in "short,long", "long,short", or "arg" format 
   * @param description The description for the argument
   * @param is_required Indicates if the input is required
   * @param validators The validators to apply to the argument
   * @param argument_policy The number of arguments to consume when parsing
   */
  inline OptionalArgument(
      string_t const& argument,
      string_t const& description=string_t(),
      bool is_required=false, 
      validator_vector_t&& validators={},
      argument_policy_t argument_policy = OptionalArgumentPolicy::none
    ) :
      OptionalArgument(detail::split_argument(argument), description, is_required, validators, argument_policy)
  {}

  /**
   * @brief Construct a new Optional Argument Object
   * 
   * @param argument The input argument, in "short,long", "long,short", or "arg" format 
   * @param description The description for the argument
   * @param default_value The default value of the parameter
   * @param validators The validators to apply to the argument
   * @param argument_policy The number of arguments to consume when parsing
   */
  template <typename T>
  requires requires (T x) {{casts::to_string(x)} -> std::same_as<string_t>; }
  OptionalArgument(
      string_t const& argument,
      string_t const& description,
      T default_value, 
      validator_vector_t&& validators={},
      argument_policy_t argument_policy = OptionalArgumentPolicy::none
    ) :
      OptionalArgument(detail::split_argument(argument), description, casts::to_string(default_value), validators, argument_policy)
  {}

  /**
   * @brief Construct a new Optional Argument object
   * 
   * @param short_argument The short argument (ex: -a)
   * @param long_argument The long argument (ex: --apple)
   * @param description The description for the argument
   * @param is_required Indicates if the input is required
   * @param validators The validators to apply to the argument
   * @param argument_policy The number of arguments to consume when parsing
   */
  OptionalArgument(
      string_t const& short_argument,
      string_t const& long_argument,
      string_t const& description, 
      bool is_required=false, 
      validator_vector_t&& validators={},
      argument_policy_t argument_policy = OptionalArgumentPolicy::none
    ) :
      BaseArgument<StringType>(description, validators),
      m_short_argument(short_argument),
      m_long_argument(long_argument),
      m_default_or_required(is_required),
      m_argument_policy(argument_policy)
  {}

  /**
   * @brief Construct a new Optional Argument object
   * 
   * @param short_argument The short argument (ex: -a)
   * @param long_argument The long argument (ex: --apple)
   * @param description The description for the argument
   * @param default_value The default value of the parameter
   * @param validators The validators to apply to the argument
   * @param argument_policy The number of arguments to consume when parsing
   */
  template <typename T>
  requires requires (T x) {{casts::to_string(x)} -> std::same_as<string_t>; }
  OptionalArgument(
      string_t const& short_argument,
      string_t const& long_argument,
      string_t const& description, 
      T default_value,
      validator_vector_t&& validators={},
      argument_policy_t argument_policy = OptionalArgumentPolicy::none
    ) :
      BaseArgument<StringType>(description, validators),
      m_short_argument(short_argument),
      m_long_argument(long_argument),
      m_default_or_required(casts::to_string(default_value)),
      m_argument_policy(argument_policy)
  {}

  /// @brief The short argument
  string_t m_short_argument;

  /// @brief The long argument
  string_t m_long_argument;

  /// @brief Defines whether the value is required, or stores some default value
  default_required_variant_t m_default_or_required;

  /// @brief The policy used to parse the command line arguments
  argument_policy_t m_argument_policy;

private:

  /// @brief Private constructor for split_argument
  explicit inline OptionalArgument [[gnu::always_inline]] (
      std::tuple<string_t, string_t> args,
      string_t const& description, 
      bool is_required=false, 
      validator_vector_t&& validators={},
      argument_policy_t argument_policy = OptionalArgumentPolicy::none
    ) :
      OptionalArgument(std::move(std::get<0>(args)), std::move(std::get<1>(args)), description, is_required, validators, argument_policy)
  {}

  /// @brief Private constructor for split_argument
  explicit inline OptionalArgument [[gnu::always_inline]] (
      std::tuple<string_t, string_t> args,
      string_t const& description, 
      string_t default_value, 
      validator_vector_t&& validators={},
      argument_policy_t argument_policy = OptionalArgumentPolicy::none
    ) :
      OptionalArgument(std::move(std::get<0>(args)), std::move(std::get<1>(args)), description, default_value, validators, argument_policy)
  {}
};

}

#endif /* INCLUDE_VARGS_ARGUMENTS_OPTIONAL_ARGUMENT */
