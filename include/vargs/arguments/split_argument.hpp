#ifndef INCLUDE_VARGS_SPLIT_ARGUMENT
#define INCLUDE_VARGS_SPLIT_ARGUMENT

#include <string>
#include <tuple>

namespace vargs {

namespace detail {
/**
 * @brief Splits an input argument into it's short and long variant
 * 
 * @param argument The input argument
 * @return std::tuple<std::string, std::string> A tuple containing the short and long arguments
 */
inline std::tuple<std::string, std::string> split_argument(std::string const& argument)
{
  std::string short_argument, long_argument;
  // Parse for short and long argument...
  return std::make_tuple(short_argument, long_argument);
}

}

}

#endif /* INCLUDE_VARGS_SPLIT_ARGUMENT */
