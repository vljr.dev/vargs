#ifndef INCLUDE_VARGS_ARGUMENTS_ARGUMENTS
#define INCLUDE_VARGS_ARGUMENTS_ARGUMENTS

#include <map>
#include <vector>

#include <vargs/arguments/optional_argument.hpp>
#include <vargs/arguments/positional_argument.hpp>

namespace vargs {

template <typename StringType=std::string>
struct GroupInfo
{
  using string_t = StringType;

  GroupInfo(string_t const& group_description=string_t()) :
    m_group_description(group_description)
  {}

  string_t m_group_description;
};

template <typename StringType=std::string>
struct Arguments
{
  using string_t = StringType;
  using optional_arguments_t = std::map<string_t, std::vector<OptionalArgument<string_t>>>;
  using positional_arguments_t = std::vector<PositionalArgument<string_t>>;

  /**
   * @brief Construct a new Arguments object
   * 
   * @param optional_arguments The optional arguments
   * @param positional_arguments The positional arguments
   */
  Arguments(
    optional_arguments_t optional_arguments=optional_arguments_t(),
    positional_arguments_t positional_arguments=positional_arguments_t()
  ) :
    m_optional(optional_arguments),
    m_positional(positional_arguments)
  {}

  /// @brief The optional parameters
  optional_arguments_t m_optional;

  /// @brief The positional parameters
  positional_arguments_t m_positional;
};

}

#endif /* INCLUDE_VARGS_ARGUMENTS_ARGUMENTS */
