#ifndef INCLUDE_VARGS_ARGUMENTS_BASE_ARGUMENT
#define INCLUDE_VARGS_ARGUMENTS_BASE_ARGUMENT

#include <memory>
#include <string>
#include <vector>

#include <vargs/argument_validators.hpp>

namespace vargs {

/**
 * @brief Stores the parameters used for all Argument Types
 * 
 * @tparam StringType 
 */
template <typename StringType=std::string>
struct BaseArgument
{

  /// @brief Stores the string type used by the Argument
  using string_t = StringType;

  /**
   * @brief Construct a new Base Argument object
   * 
   * @param description The description
   * @param validators The validators used
   */
  BaseArgument(string_t const& description=string_t(), validator_vector_t&& validators={}) :
    m_description(description),
    m_validators(validators)
  {}

  /// @brief The description of the argument
  string_t m_description;

  /// @brief The list of validators to run on the argument
  validator_vector_t m_validators;

protected:
  /// @details We are not storing Arguments by it's base class/pointer to the base class, so make the destructor protected...
  ~BaseArgument() = default;
};

}

#endif /* INCLUDE_VARGS_ARGUMENTS_BASE_ARGUMENT */
