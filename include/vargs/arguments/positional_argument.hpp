#ifndef INCLUDE_VARGS_ARGUMENTS_POSITIONAL_ARGUMENT
#define INCLUDE_VARGS_ARGUMENTS_POSITIONAL_ARGUMENT

#include <cstdint>
#include <memory>
#include <string>
#include <variant>
#include <vector>

#include <vargs/arguments/base_argument.hpp>
#include <vargs/argument_validators.hpp>

namespace vargs {

/**
 * @brief The policies supported for positional arguments
 * 
 */
enum class PositionalArgumentPolicy : uint16_t
{
  one, // One argument only
  append, // Append an endless number of arguments
  append_to_delim // Appends until either the end is reached, or a delimiter is reached
};

using positional_argument_policy_t = std::variant<std::size_t, PositionalArgumentPolicy>;

/**
 * @brief The positional argument 
 * 
 */
template <typename StringType=std::string>
struct PositionalArgument : public BaseArgument<StringType>
{
  using string_t = typename BaseArgument<StringType>::string_t;

  using argument_policy_t = positional_argument_policy_t;

  PositionalArgument(
    std::string const& argument=string_t(), 
    std::string const& description=string_t(),
    validator_vector_t validators={}
  ) :
    BaseArgument<StringType>(description, validators),
    m_argument(argument)
  {}

  std::string m_argument;
};

}

#endif /* INCLUDE_VARGS_ARGUMENTS_POSITIONAL_ARGUMENT */
