#ifndef INCLUDE_VARGS_ARGUMENT_VALIDATORS
#define INCLUDE_VARGS_ARGUMENT_VALIDATORS

#include <concepts>
#include <filesystem>
#include <memory>
#include <string_view>
#include <span>
#include <vector>

#include <vargs/casts/casts.hpp>
#include <vargs/error_msg.hpp>

namespace vargs {

// todo: replace with CRTP

/**
 * @brief Defines a base validator object
 * 
 */
struct BaseValidator
{
  /**
   * @brief Construct a new Base Validator object
   * 
   */
  BaseValidator() = default;

  /**
   * @brief Destroy the Base Validator object
   * 
   */
  virtual ~BaseValidator() = default;

  /**
   * @brief Validates the input argument
   * 
   * @param arg The input terminal argument
   *
   * @return true The parameter was validated
   * @return false The parameter was not validated
   */
  virtual bool validate(std::string_view arg) { return true; };

  /// @brief The error message
  error_message_t m_error_message;
};

using validator_vector_t = std::vector<std::unique_ptr<BaseValidator>>;

/**
 * @brief Indicates if values exist within 
 * 
 * @tparam T 
 */
template <typename T>
class ValidatorIsContainedIn : public BaseValidator
{

public:

  /**
   * @brief Construct a new Is Contained In object
   * 
   * @param args_to_test 
   */
  ValidatorIsContainedIn(std::span<T> args_to_test) : m_args_to_test(args_to_test) {}

  bool validate(std::string_view arg) override
  {
    // todo: create cast to correct T value
  }

private:
  std::span<T> m_args_to_test;
};

template <std::floating_point T>
class ValidatorIsCloseTo : public BaseValidator
{

};

// File IO
class ValidatorIsFile;
class ValidatorIsDirectory;
class ValidatorIsPath;
class ValidatorParentDirectoryExists;

// Numerical Tests
template <utils::Numerical T>
class ValidatorIsWithinRange;

template <typename EnumType>
class ValidatorIsWithinEnum;



}

#endif /* INCLUDE_VARGS_ARGUMENT_VALIDATORS */
