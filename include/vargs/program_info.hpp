#ifndef INCLUDE_VARGS_PROGRAM_INFO
#define INCLUDE_VARGS_PROGRAM_INFO

#include <string>

namespace vargs {

/**
 * @brief A struct wrapping around program information
 * 
 */
template <typename StringType=std::string>
struct ProgramInfo
{

  ProgramInfo(
    StringType const& program_name=StringType(), 
    StringType const& program_version=StringType(), 
    StringType const& program_desc=StringType(),
    StringType const& program_hash=StringType(),
    StringType const& program_architecture=StringType()
  ) :
    m_program_name(program_name),
    m_program_version(program_version),
    m_program_desc(program_desc),
    m_program_hash(program_hash),
    m_program_architecture(program_architecture)
  {}

  /// @brief The name of the program
  StringType m_program_name;

  /// @brief The version of the program
  StringType m_program_version;

  /// @brief The description of the program
  StringType m_program_desc;

  /// @brief The git hash of the program
  StringType m_program_hash;

  /// @brief The architecture the software was compiled to
  StringType m_program_architecture;
};

}

#endif /* INCLUDE_VARGS_PROGRAM_INFO */
