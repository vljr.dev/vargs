#ifndef INCLUDE_VARGS_ARGUMENT_PARSER
#define INCLUDE_VARGS_ARGUMENT_PARSER

#include <string>
#include <string_view>
#include <type_traits>
#include <variant>
#include <vector>

#include <vargs/arguments/arguments.hpp>
#include <vargs/argument_format_messages.hpp>
#include <vargs/argument_parser_preferences.hpp>
#include <vargs/argument_result.hpp>
#include <vargs/argument_validators.hpp>
#include <vargs/error_msg.hpp>
#include <vargs/program_info.hpp>

namespace vargs {

namespace detail {

template <typename StringType=std::string>
struct HelpMessageComponents
{
  using string_t = StringType;

  HelpMessageComponents(string_t const& description=string_t(), string_t const& epilog=string_t()) :
    m_description(description),
    m_epilog(epilog)
  {}

  /// @brief The description for the help message
  string_t m_description;

  /// @brief The epilog for the help message
  string_t m_epilog;
};

template <typename ResultType=ArgumentResult, typename EnableExceptions=std::false_type, typename StringType=std::string>
class ArgumentParser
{
  using class_t = ArgumentParser<ResultType, EnableExceptions, StringType>;

public:
  /// @brief The Argument Result Type
  using result_t = ResultType;

  /// @brief Indicates if the class is throwing exceptions, or returning an error message
  static constexpr bool const using_exceptions=EnableExceptions::value;

  /// @brief The string type used in parsing the arguments
  using string_t = StringType;

  /// @brief The datatype used to store the program info
  using program_info_t = ProgramInfo<string_t>;

  /// @brief The datatype used to store the arguments
  using arguments_t = Arguments<string_t>;

  /// @brief The datatype used to store an optional argument
  using optional_argument_t = OptionalArgument<string_t>;

  /// @brief The datatype used to store a positional argument
  using positional_argument_t = PositionalArgument<string_t>;

  /// @brief The datatype used to store the format messages
  using format_messages_t = ArgumentFormatMessages<string_t>;

  /// @brief The help message components used
  using help_message_components_t = HelpMessageComponents<string_t>;

  /// @brief Defines the Argument Parser Preferences
  using preferences_t = ArgumentParserPreferences<string_t>;

  /// @brief Defines the group info type
  using group_info_t = GroupInfo<string_t>;

  /// @brief map<group_name, group_info>
  using groups_t = std::map<string_t, group_info_t>;

  /// @brief The errors encountered when performing 
  using error_messages_t = std::vector<error_message_t>;

  /**
   * @brief Construct a new Argument Parser object
   * 
   * @param program_info The program information
   * @param description The description of the program
   * @param add_help_option Add 
   * @param add_version_option 
   * @param add_program_info_option 
   * @param formatters 
   */
  ArgumentParser(
    program_info_t const& program_info,
    string_t const& description=string_t(),
    format_messages_t const& formatters=format_messages_t(),
    preferences_t const& preferences=preferences_t()
  ) : 
    m_program_info(program_info),
    m_help_message_components(description, ""),
    m_arguments(),
    m_formatters(formatters),
    m_preferences(preferences),
    m_groups()
  {}

  ArgumentParser(
    program_info_t const& program_info,
    help_message_components_t const& help_message_components,
    format_messages_t const& formatters=format_messages_t(),
    preferences_t const& preferences=preferences_t()
  ) : 
    m_program_info(program_info),
    m_help_message_components(help_message_components),
    m_arguments(),
    m_formatters(formatters),
    m_preferences(preferences),
    m_groups()
  {}

  virtual ~ArgumentParser() = default;

  /**
   * @brief Adds a group to the argument parser
   * 
   * @param group The group
   * @param info The information for the particular group
   *
   * @return class_t& This object
   */
  class_t& addGroup(string_t const& group, group_info_t const& info)
  {
    m_groups[group] = info;
  }

  /**
   * @brief Adds a group to the argument parser
   * 
   * @param group The group
   * @param args The arguments to pass into group_info_t
   *
   * @return class_t& This object
   */
  template <typename... Args>
  requires requires (Args... args) { group_info_t(args...); }
  class_t& addGroup(string_t const& group, Args... args)
  {
    m_groups[group] = group_info_t(args...);
  }

  /**
   * @brief Adds an optional flag for help messages
   * 
   * @param description The description to use for the help message
   * @param group The group name associated with the argument
   *
   * @return class_t& This class
   */
  class_t& addHelpFlag(string_t const& description="Prints out this help message", string_t const& args="h,help", string_t const& group=preferences_t::m_default_info_group) noexcept 
  { 
    return addOptionalFlag(args, description, OptionalArgumentPolicy::show_help, group);
  }

  /**
   * @brief Add an optional flag for version messages
   * 
   * @param description The description to use for the version message
   * @param group The group name associated with the argument
   *
   * @return class_t& This class
   */
  class_t& addVersionFlag(string_t const& description="Prints out the version of the software", string_t const& args="version", string_t const& group=preferences_t::m_default_info_group) noexcept 
  { 
    return addOptionalFlag(args, description, OptionalArgumentPolicy::show_version, group);
  }

  /**
   * @brief Add an optional flag for outputting program information 
   * 
   * @param description The description to use for the program info message
   * @param group The group name associated with the argument
   *
   * @return class_t& This class
   */
  class_t& addProgramInfoFlag(string_t const& description="Prints out the program info of the software", string_t const& args="program-info", string_t const& group=preferences_t::m_default_info_group) noexcept 
  {
    return addOptionalFlag(args, description, OptionalArgumentPolicy::show_program_info, group);
  }

  /**
   * @brief Adds an optional boolean flag option
   * 
   * @param argument The name of the argument, in {short,long}, {long,short}, or {long} format
   * @param description The description to use for the flag
   * @param group The group name associated with the argument
   *
   * @return class_t& This class
   */
  class_t& addOptionalFlag(string_t const& argument, string_t const& description=string_t(), string_t const& group=string_t()) noexcept
  {
    return addOptionalFlag(argument, description, OptionalArgumentPolicy::none, group);
  }

  /**
   * @brief Adds an optional boolean flag option
   * 
   * @param short_argument The short argument
   * @param long_argument The long argument
   * @param description The description to use for the flag
   * @param group The group name associated with the argument
   *
   * @return class_t& This class
   */
  class_t& addOptionalFlag(string_t const& short_argument, string_t const& long_argument, string_t const& description=string_t(), string_t const& group=string_t()) noexcept
  {
    return addOptionalFlag(short_argument, long_argument, description, OptionalArgumentPolicy::none, group);
  }

  /**
   * @brief Adds an optional argument
   * 
   * @param argument The argument to add
   * @param description The description of the argument
   * @param group The group name associated
   * @param is_required Indicates if the argument is required
   * @param validators Any validators to apply to the result
   * @param argument_policy The argument policy to apply
   *
   * @return class_t& This class
   */
  class_t& addOptionalArgument(
      string_t const& argument,
      string_t const& description=string_t(),
      string_t const& group=string_t(),
      bool is_required=false, 
      validator_vector_t&& validators={},
      optional_argument_policy_t argument_policy = OptionalArgumentPolicy::one
  )
  {
    return addOptionalArgumentImpl(group, argument, description, is_required, validators, argument_policy);
  }

  /**
   * @brief Adds an optional argument
   * 
   * @tparam T Some type that can be cast to a string using casts::to_string(default_value)
   *
   * @param argument The argument to add
   * @param description The description of the argument
   * @param group The group name associated
   * @param default_value The default value used by the argument
   * @param validators Any validators to apply to the result
   * @param argument_policy The argument policy to apply
   *
   * @return class_t& This class
   */
  template <typename T>
  class_t& addOptionalArgument(
    string_t const& argument,
    string_t const& description,
    string_t const& group,
    T default_value,
    validator_vector_t&& validators={},
    optional_argument_policy_t argument_policy = OptionalArgumentPolicy::one
  )
  {
    return addOptionalArgumentImpl(group, argument, description, default_value, validators, argument_policy);
  }

  /**
   * @brief Adds an optional argument
   * 
   * @param arg The optional argument
   * @param group The group to place the optional argument
   *
   * @return class_t& This class
   */
  class_t& addOptionalArgument(
    optional_argument_t&& arg,
    string_t const& group=string_t()
  )
  {
    addOptionalArgument(group, std::move(arg));
  }

  template <typename... Args>
  requires requires (Args... args) { positional_argument_t(args...); }
  class_t& addPositionalArgument(Args... args) noexcept
  {
    positional_argument_t arg(args...);
    m_arguments.m_positional.push_back(std::move(arg));
    return *this;
  }

  /**
   * @brief Gets/Sets the formatters used by the Argument Parser
   * 
   * @return ArgumentFormatMessages& The Argument Formatters
   */
  ArgumentFormatMessages<std::string_view>& formatters() noexcept { return m_formatters; }

  /**
   * @brief Gets the formatters used by the Argument Parser
   * 
   * @return ArgumentFormatMessages const& The Argument Formatters
   */
  ArgumentFormatMessages<std::string_view> const& formatters() const noexcept { return m_formatters; }

  /**
   * @brief Prints debug information about the argparser
   * 
   * @param tab_level The number of tabs to apply to the beginning
   * @param tab The tab character to use
   *
   * @return std::string Debug information about the argument parser
   */
  string_t info(std::size_t tab_level=0, string_t const& tab="    ") const
  {
    std::string base_tab;
    for (std::size_t i=0; i < tab_level; ++i) { base_tab+=tab; }

    // todo: finish

    return "";
  }

  /**
   * @brief Parses the input terminal arguments
   * 
   * @param argc The number of arguments
   * @param argv The arguments
   *
   * @return parse_result_t The parsed results
   */
  result_t parse(int argc, char const * const * argv) const;

  /**
   * @brief Parses the input terminal arguments, passed in as an array of string_view
   * 
   * @param args The terminal arguments
   *
   * @return result_t 
   */
  result_t parse(std::span<std::string_view> const args) const;

  /**
   * @brief Parses the input terminal arguments, passed in as an array of strings
   * 
   * @param args The terminal arguments
   *
   * @return result_t 
   */
  result_t parse(std::span<std::string> const args) const;

  /**
   * @brief The program information
   * 
   * @return ProgramInfo const& The program information
   */
  ProgramInfo<>& programInfo() noexcept { return m_program_info; }

  /**
   * @brief The program information
   * 
   * @return ProgramInfo const& The program information
   */
  ProgramInfo<> const& programInfo() const noexcept { return m_program_info; }

private:
  /// @brief The program information
  program_info_t m_program_info;

  /// @brief The help message components
  help_message_components_t m_help_message_components;

  /// @brief The arguments to parse
  arguments_t m_arguments;

  /// @brief The formatters
  format_messages_t m_formatters;

  /// @brief The argument parser preferences
  preferences_t m_preferences;

  /// @brief The groups in the argument parser
  groups_t m_groups;

  /// @brief The error messages
  error_messages_t m_error_messages;

  /**
   * @brief Adds an optional boolean flag option
   * 
   * @param argument The name of the argument, in {short,long}, {long,short}, or {long} format
   * @param description The description to use for the flag
   *
   * @return class_t& This class
   */
  inline class_t& addOptionalFlag(string_t const& argument, string_t const& description, OptionalArgumentPolicy const& policy, string_t const& group) noexcept
  {
    return addOptionalArgumentImpl(group, argument, description, false, {}, policy);
  }

  /**
   * @brief Adds an optional boolean flag option
   * 
   * @param short_argument The short argument
   * @param long_argument The long argument
   * @param description The description to use for the flag
   *
   * @return class_t& 
   */
  inline class_t& addOptionalFlag(string_t const& short_argument, string_t const& long_argument, string_t const& description, OptionalArgumentPolicy const& policy, string_t const& group) noexcept
  {
    return addOptionalArgumentImpl(group, short_argument, long_argument, description, false, {}, policy);
  }

  /**
   * @brief Creates a group if it doesn't exist already
   * 
   * @param group The group
   */
  void touchGroup(string_t const& group)
  {
    m_groups[group];
  }

  /**
   * @brief Basic implementation behind adding an optional argument
   * 
   * @tparam Args 
   * @param group 
   * @param args 
   * @return class_t& 
   */
  template <typename... Args>
  requires requires (Args... args) { optional_argument_t(args...); }
  class_t& addOptionalArgumentImpl(string_t const& group, Args... args)
  {
    touchGroup(group);
    optional_argument_t arg(args...);
    m_arguments.m_optional[group].push_back(std::move(arg));
    return *this;
  }
};

}

/// @brief Defines an alias around detail::ArgumentParser for direct boolean input
template <typename ResultType=ArgumentResult, bool EnableExceptions=false>
using ArgumentParser = detail::ArgumentParser<ResultType, std::integral_constant<bool, EnableExceptions>, std::string>;

}

#endif /* INCLUDE_VARGS_ARGUMENT_PARSER */
