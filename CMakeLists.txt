cmake_minimum_required(VERSION 3.10.0)

# Project information
set(PROJECT_NAME "VARGS_DIR")
set(PROJECT_DESCRIPTION "Defines the means for parsing terminal arguments in modern c++")
project(${PROJECT_NAME})

# Prevent insource builds
if(${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
  message(FATAL_ERROR "In-source builds not allowed. Please make a new directory (called a build directory) and run CMake from there. You may need to remove CMakeCache.txt. ")
endif()

# Set the library version
set(_MAJOR_VERSION 0)
set(_MINOR_VERSION 1)
set(_PATCH_VERSION 0)
set(VERSION ${_MAJOR_VERSION}.${_MINOR_VERSION}.${_PATCH_VERSION}) 

# Set the library structure
set(INCLUDE_DIR ${CMAKE_SOURCE_DIR}/include)
set(VARGS_DIR ${INCLUDE_DIR}/VARGS_DIR)
set(TESTS_DIR ${INCLUDE_DIR}/tests)

# Prevents bad build type strings
if (NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE "Release")
endif()

# Configure options
option (DISABLE_LOCAL_TESTS OFF)
option (DISABLE_DOCKER_TESTS OFF)
option (DISABLE_DOXYGEN OFF)
option (DISABLE_ENVIRONMENT_MODULE OFF)

# Functions
function (create_exe target_name target_path flags includes linkers)
add_executable("${target_name}" "${target_path}/${target_name}.cpp")
separate_arguments(sep_includes UNIX_COMMAND "${includes}")
target_include_directories(${target_name} PUBLIC ${sep_includes})
separate_arguments(sep_flags UNIX_COMMAND "${flags}")
target_compile_options(${target_name} PUBLIC ${sep_flags})
separate_arguments(sep_linkers UNIX_COMMAND "${linkers}")
target_link_libraries(${target_name} ${sep_linkers})
endfunction()

function (create_test test_name target_path flags includes linkers)
message(STATUS "Creating test ${test_name}")
create_exe("${test_name}" "${target_path}" "${flags}" "${includes}" "${linkers}")
add_test(NAME "${test_name}" COMMAND "${test_name}")
endfunction()

# Local tests
if (NOT DISABLE_LOCAL_TESTS)
  message("Local tests enabled")
  set(test_src_path "${TESTS_DIR}/src")
  set(test_compile_opts "-std=c++20 -g")
  set(test_include_path "${TESTS_DIR}/include ${INCLUDE_DIR}")
  set(test_link_libraries "-lgtest")
else()
  message("Local tests disabled")
endif()

# Create install rule
install(DIRECTORY ${VARGS_DIR} DESTINATION ${CMAKE_INSTALL_PREFIX}/include/vargs) 